## Тестовый проект

Команда для запуска:
```bash
# Для установки всех нужных пакетов
composer update
npm install

# Запуск
docker compose up -d

# Через секунд 10-15 после запуска mysql
php artisan migrate
php artisan db:seed
```
! Важно, контейнер монтирует файловую систему хоста

Документация к апи есть в корне в файле api.yams в формате OpenAPI. Любой запрос к апи должен содержать заголовок Accept application/json, иначе аутентификация не пройдёт