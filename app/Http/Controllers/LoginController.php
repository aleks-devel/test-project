<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request) {
        $data = $request->validate([
            "email" => "required|email",
            "password" => "required"
        ]);

        $user = User::query()->where("email", $data["email"])->firstOrFail();
        $token = auth()->login($user);

        if(!$token) {
            return response()->json(["error" => "Unauthorized"], 401);
        }

        return $token;
    }
}
