<?php

namespace App\Http\Controllers;

use App\Http\Resources\SearchDataResource;
use App\Models\SearchData;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class MainController extends Controller {
    public function getMain(Request $request) {
        if ($request->has("search")) {
            $search = $request->get("search");

            session(["search" => $search]);

            try {
                $repo = $this->getRepo($search);
                return view("main-page", ["repo" => $repo->resolve()]);
            } catch (BadRequestException $e) {
                return view("main-page")->withErrors(["cause" => $e->getMessage()]);
            }
        }

        return view("main-page");
    }

    public function postFind(Request $request): SearchDataResource {
        // В ТЗ была строка поиска напрямую в body ¯\_( ͡• ͜ʖ ͡•)_/¯
        $search = $request->getContent();

        return $this->getRepo($search);
    }

    private function getRepo($search): SearchDataResource {
        $search = (string)$search;

        if(strlen($search) === 0) {
            throw new BadRequestException("search.small.length");
        } else if (strlen($search) > 255) {
            throw new BadRequestException("search.big.length");
        }

        $existingData = SearchData::firstWhere("search", $search);

        if ($existingData !== null) {
            return new SearchDataResource($existingData);
        }

        $response = Http::get("https://api.github.com/search/repositories?q={$search}");

        $responseCode = $response->status();

        if ($responseCode !== 200) {
            throw new BadRequestException("github.code");
        }

        $data = $response->body();

        $repo = new SearchData();
        $repo->search = $search;
        $repo->data = $data;
        $repo->save();

        return new SearchDataResource($repo);
    }

    public function getFind(Request $request): AnonymousResourceCollection {
        $pagination = $request->get("pagination", false);

        if ($pagination) {
            $pageSize = $request->get("page_size", 20);

            return SearchDataResource::collection(SearchData::paginate($pageSize));
        } else {
            return SearchDataResource::collection(SearchData::all());
        }
    }

    public function deleteFind(string $id): void {
        SearchData::destroy($id);
    }
}
