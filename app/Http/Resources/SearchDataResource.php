<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SearchDataResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        $data = json_decode($this->data, true);
        $items = $data["items"];
        $response = [];

        foreach ($items as $item) {
            $response[] = [
                "name" => $item["name"],
                "author" => [
                    "name" => $item["owner"]["login"],
                    "ava" => $item["owner"]["avatar_url"],
                ],
                "url" => $item["html_url"],
                "starts" => $item["stargazers_count"],
                "watchers" => $item["watchers_count"]
            ];
        }

        return [
            "id" => $this->id,
            "items" => $response
        ];
    }
}
