<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         User::insert([
             "name" => "Test User",
             "email" => "test@test",
             "password" => password_hash("test@test", PASSWORD_DEFAULT)
         ]);
    }
}
