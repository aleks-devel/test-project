<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ asset("css/app.css") }}">
  @vite(["resources/css/app.css"])
  @vite(["resources/js/app.js"])
</head>
<body>

<div class="container mt-5">
  <h2 class="text-center">Тестовое задание</h2>
  <div class="mt-4">
    <form action="">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Поиск" name="search" value="{{ session("search", "") }}">
        <div class="input-group-append">
          <button type="submit" class="btn btn-outline-primary">
            Найти<i class="ml-2 fa fa-search"></i>
          </button>
        </div>
        @error("cause")
          <span class="invalid-feedback d-block h5">
            @if($message === "search.big.length")
              Слишком длинный запрос
            @elseif($message === "search.small.length")
              Поиск идёт от 1 символа
            @elseif($message === "github.code")
              Гитхаб вернул ошибку
            @endif
          </span>
        @enderror
      </div>
    </form>
  </div>
  @if(isset($repo))
    <div class="row">
      @foreach($repo["items"] as $item)
        <div class="col-3 mt-4">
          <a href="{{ $item["url"] }}" class="card" target="_blank">
            <div class="card-body">
              <h5 class="card-title">{{ $item["name"] }}</h5>
              <div class="card-text">
                <p>
                  <img src="{{ $item["author"]["ava"] }}" alt="{{ $item["author"]["name"] }}" class="ava mr-2">
                  {{ $item["author"]["name"] }}
                </p>
                <p class="m-0">Звёзд: {{ $item["starts"] }}</p>
                <p class="m-0">Просмотров: {{ $item["watchers"] }}</p>
              </div>
            </div>
          </a>
        </div>
      @endforeach
    </div>
  @endif
</div>

</body>
</html>